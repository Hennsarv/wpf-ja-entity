﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfEntity
{
    /// <summary>
    /// Interaction logic for Window2.xaml
    /// </summary>
    public partial class Window2 : Window
    {
        NorthwindEntities ne = new NorthwindEntities();
        public List<Categories> Kategooriad = null;
        
        

        public Window2()
        {
            InitializeComponent();

            Kategooriad = ne.Categories.ToList();

            //this.Puuvaade.Items.Clear();
            this.Puuvaade.ItemsSource = Kategooriad;


        }
    }
}
